@extends('layouts.master')

@section('title', 'My Home Page')

@section('content')
<div class="container">
<style>
table, th, td {
  border: 1px solid white;
  padding: 10px;
}
table {
  border-spacing: 15px;
  text-align: center;
}
th {
    text-align: center;
}
</style>
    <div class="container">
      <h2>Create A proposal</h2><br  />
      <a href="{{ url('/products') }}">view a proposal</a>
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
      </div><br />
      @endif
      <!-- The form method used to get around the composer error-->
      <form method="post" action="{{url('products')}}">
        {{csrf_field()}}
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="test">test:</label>
              <textarea placeholder="enter test"
                        style="resize: vertical"
                        id="test"
                        name="test"
                        rows="5" spellcheck="false"
                        class="form-control">
                      </textarea>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="projecttitle">projecttitle:</label>
              <input type="text" class="form-control" name="projecttitle">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="startdate">start date:</label>
              <input type="date" class="form-control" name="startdate">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="enddate">enddate:</label>
              <input type="date" class="form-control" name="enddate">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="researchers">researchers:</label>
              <input type="text" class="form-control" name="researchers">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="summary">summary:</label>
              <textarea placeholder="enter summary"
                        style="resize: vertical"
                        id="summary"
                        name="summary"
                        rows="5" spellcheck="false"
                        class="form-control">
                      </textarea>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="participants">participants:</label>
              <input type="text" class="form-control" name="participants">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
              <div class="form-group col-md-4">
                <label for="methodology">methodology:</label>
                <textarea placeholder="enter methodology"
                          style="resize: vertical"
                          id="methodology"
                          name="methodology"
                          rows="5" spellcheck="false"
                          class="form-control">
                        </textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                  <label for="riskassessment">risk assessment:</label>
                  <textarea placeholder="enter risk assessment"
                            style="resize: vertical"
                            id="riskassessment"
                            name="riskassessment"
                            rows="5" spellcheck="false"
                            class="form-control">
                          </textarea>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4"></div>
                  <div class="form-group col-md-4">
                    <label for="datastorageandconfidentiality">data storage and confidentiality:</label>
                    <textarea placeholder="enter data storage and confidentiality"
                              style="resize: vertical"
                              id="datastorageandconfidentiality"
                              name="datastorageandconfidentiality"
                              rows="5" spellcheck="false"
                              class="form-control">
                            </textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                      <label for="compensator">compensator:</label>
                      <textarea placeholder="enter compensator"
                                style="resize: vertical"
                                id="compensator"
                                name="compensator"
                                rows="5" spellcheck="false"
                                class="form-control">
                              </textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4"></div>
                      <div class="form-group col-md-4">
                        <label for="informedconsent">informed consent:</label>
                        <textarea placeholder="enter informed consent"
                                  style="resize: vertical"
                                  id="informedconsent"
                                  name="informedconsent"
                                  rows="5" spellcheck="false"
                                  class="form-control">
                                </textarea>
                      </div>
                    </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Add Product</button>
          </div>
        </div>
      </form>
    </div>
