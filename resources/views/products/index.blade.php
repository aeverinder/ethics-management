<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <a href="{{ url('/products/create') }}">Upload ethics form</a>
    <div class="container">
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
      </div><br />
      @endif
    <!-- <table class="table table-striped"> -->


        <!-- <th colspan="2">Action</th> -->
        <div class="container">
      @foreach($products as $product)
      <div class="panel-heading">ID:  {{$product['id']}}</div>
        <div class="panel-body">
        <p>name:  {{$product['name']}}</p>
        <p>test:  {{$product['test']}}</p>
        <p>project title: {{$product['projecttitle']}}</p>
          <button class=find"><a href="{{asset($product->proposal_link)}}">View proposal</a></button>
        </div>
      </div>
          <form action="{{action('ProductController@destroy', $product['id'])}}" method="post">
            {{csrf_field()}}
            <td><a href="{{action('ProductController@edit', $product['id'])}}" class="btn btn-warning">Edit</a></td>
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
