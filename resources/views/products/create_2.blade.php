@extends('layouts.master')

@section('title', 'My Home Page')

@section('content')
<!--Form upload method being called -->
    {!! Form::open(array('route' => 'product.upload.post','files'=>true, 'enctype' => "multipart/form-data")) !!}

    {!! Form::file('proposal', array('class' => 'form-control')) !!}

    <div class="form-group col-md-4">
        <button type="submit" class="btn btn-success" style="margin-left:38px">Add file</button>
    </div>
  {!! Form::close() !!}

@endsection
