@extends('layouts.master')

@section('title', 'My Home Page')

@section('content')
<div class="container">
  <!--style section -->
<style>
table, th, td {
  border: 1px solid white;
  padding: 10px;
}
table {
  border-spacing: 15px;
  text-align: center;
}
th {
    text-align: center;
}
</style>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
              <!--heading with a link allowing the user to go back to the edge hill  -->
                <div class="panel-heading"><a href="https://www.edgehill.ac.uk/" title="Edge Hill University" tabindex="-1">Edge Hill University</a></div>
                <!--body with text from edgehill university talking about the ethics
              framework with later a link to the page showing alll of the docs needed to
            complete the process -->
                <div class="panel-body">
                  The University has a governance framework within which all members
                  of Edge Hill community must act when engaging in research or knowledge exchange.

                  The Research Ethics Policy and the Code of Practice for the Conduct of Research are the principal
                  documents for guiding researchers in ethical conduct.
                  <br>
                  <a href="https://www.edgehill.ac.uk/research/governance/?tab=ethics-and-risk" title="Edge Hill University" tabindex="-1">Edge Hill University ethic documents page</a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
