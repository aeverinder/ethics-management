<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/product', 'ProductController@index')->name('product');
Route::get('/products/create', 'ProductController@create')->name('product');

//post method
Route::post('product-upload',['as'=>'product.upload.post','uses'=>'ProductController@image_upload']);

Route::resource('/product', 'ProductController');
//Route::post('/products', 'ProductController@image_upload');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'web'], function () {
    Route::auth();
  });
