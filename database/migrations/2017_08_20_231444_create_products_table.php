<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('test',1000);
            $table->string('projecttitle',1000);
            $table->date('startdate',1000);
            $table->date('enddate',1000);
            $table->string('researchers',1000);
            $table->string('summary',1000);
            $table->string('participants',1000);
            $table->string('methodology',1000);
            $table->string('riskassessment',1000);
            $table->string('datastorageandconfidentiality',1000);
            $table->string('compensator',1000);
            $table->string('informedconsent',1000);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
