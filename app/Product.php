<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  //fillable feilds within the database
  
    protected $fillable = ['name',
    'test',
    'projecttitle',
    'startdate',
    'enddate',
    'researchers',
    'summary',
    'participants',
    'methodology',
    'riskassessment',
    'datastorageandconfidentiality',
    'compensator',
    'informedconsent'
  ];
}
