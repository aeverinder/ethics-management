<?php

namespace App\Http\Controllers;
use App\Product;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all()->toArray();
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create_2');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function image_upload(Request $request){
        //for security i am naming the file by time of upload
        $ProposalName = time() . '.' . request()->proposal->getClientOriginalExtension();
        request()->proposal->move(public_path('storage/proposal'), $ProposalName);



        // choosing where the file will be uploaded to
        $proposal = 'storage/proposal' . $ProposalName;
        return view('/home');
    }
    public function store(Request $request)
    {
        $product = $this->validate(request(), [
          'name' => 'required',
          'test' => 'required',
          'projecttitle' => 'required',
          'startdate' => 'required',
          'enddate' => 'required',
          'researchers' => 'required',
          'summary' => 'required',
          'participants' => 'required',
          'methodology' => 'required',
          'riskassessment' => 'required',
          'datastorageandconfidentiality' => 'required',
          'compensator' => 'required',
          'informedconsent' => 'required',

        ]);
        Product::create($product);
        return back()->with('success', 'Product has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('products.edit',compact('product','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $this->validate(request(), [
          'name' => 'required',
          'test' => 'required',
          'projecttitle' => 'required',
          'startdate' => 'required',
          'enddate' => 'required',
          'researchers' => 'required',
          'summary' => 'required',
          'participants' => 'required',
          'methodology' => 'required',
          'riskassessment' => 'required',
          'datastorageandconfidentiality' => 'required',
          'compensator' => 'required',
          'informedconsent' => 'required',
        ]);
        $product->name = $request->get('name');
        $product->test = $request->get('test');
        $product->projecttitle = $request->get('projecttitle');
        $product->startdate = $request->get('startdate');
        $product->enddate = $request->get('enddate');
        $product->researchers = $request->get('researchers');
        $product->summary = $request->get('summary');
        $product->participants = $request->get('participants');
        $product->methodology = $request->get('methodology');
        $product->riskassessment = $request->get('riskassessment');
        $product->datastorageandconfidentiality = $request->get('datastorageandconfidentiality');
        $product->compensator = $request->get('compensator');
        $product->informedconsent = $request->get('informedconsent');

        $product->save();
        return redirect('products')->with('success','Product has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect('products')->with('success','Product has been  deleted');
    }
}
