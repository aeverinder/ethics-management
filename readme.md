<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

Installation of Ethics management system
To download Ethics management system from bit bucket run git clone
https://aeverinder@bitbucket.org/aeverinder/ethics-management.git
Once the project download run cd ethics-management in bash
Once in the ethics-management directory run composer update
Once composer has finished updating locate the .env.example file within the project and change the name to .env
Change .env file DB_Database to dissertation_database
change DB_USERNAME to root
change DB_PASSWORD=webdev
Close the file
Run php artisan serve in bash
Open internet browser and navigate to localhost:8000/home to make sure the application runs
In bash enter ctrl + c to stop local host from running
Then to install the Database
log into my sql server
If the username and password is different then change the .env file to match
Create new schema and call it ethics-management
data collation utf8- default and click apply
Back in the bask run php artisan migrate
This should migrate all of the right migrations into the Database
